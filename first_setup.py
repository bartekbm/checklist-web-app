try:
    from config.config_parser import check_config, delete_config, read_default_config, save_new_config_file, \
        generate_random_password_save_to_file, read_seed_password

except Exception as e:
    print(e, "Probably virtual environment not started, first start it with: 'source ./virtual_env/bin/activate' and"
          " then run: 'python first_setup.py'")
    exit(-1)
from config.generate_start_file import save_file
from tools.commands import database
print('''
Welcome to Checklist app, this is setup section for app.
First scripts is checking if configuration.ini exist or have required section and options. You can initiate new
configuration there.
Three steps available:
- Initiate or re-create configuration, after script need to exit.
- Recreate tables in database, it will DELETE all content in database. After script will exit.
- Create start_app.sh to run app.
''')


def input_for_config():
    print("DELETING FILE ...")
    delete_config()
    read = read_default_config()
    save = save_new_config_file(read)
    if save:
        print('New config saved ... \nScript exiting, run again to use other options')
        exit(-1)


def user_decision(text, to_run=''):
    user_decision_input = 'str'
    while user_decision_input.lower() != 'y' and user_decision_input.lower() != 'n':
        user_decision_input = input(text)
        if user_decision_input.lower() == 'q':
            print('STOPPING SCRIPT')
            exit(1)
        if user_decision_input.lower() == 'y':
            if to_run == 'config':
                input_for_config()
            if to_run == 'database':
                print("Generating new admin password, this will be save do app folder in "
                      "seed_admin_password_save_it_AND_DELETE_FILE file, read this file and delete after. If you don't "
                      "anybody who has access to server will be admin of this app :)")
                password = generate_random_password_save_to_file()
                read_pass = read_seed_password(password)
                new_pass = read_pass
                database.reset(new_pass)
            if to_run == 'basis_file':
                save_file()

            break
        if user_decision_input.lower() == 'n':
            break


def db_init():
    text = "Initialize new table (will delete all tables in db if exists) ? Y/N. Type Q to exit."
    user_decision(text, to_run='database')


def initialize_basic_settings():
    text = "Create new start file (will delete file if exist) ? Y/N. Type Q to exit."
    user_decision(text, to_run='basis_file')


if check_config() is True:
    print('Config with required options already exist')
    text = "Create new file (will delete file if exist) ? Y/N. Type Q to exit."
    user_decision(text, to_run='config')

else:
    print('Problem with config file.', check_config())
    text = "Create new file (will delete file if exist) ? Y/N. Type Q to exit."
    user_decision(text, to_run='config')

db_init()
initialize_basic_settings()
