#!/bin/bash

pythonv3bin=$( ls -1 /usr/bin/python* 2>/dev/null | grep '[3].[6-9]$' | xargs -r | awk '{print $1}' )
pythonv3binlocal=$( ls -1 /usr/local/bin/python3* 2>/dev/null | grep '[3].[6-9]$' | xargs -r | awk '{print $1}' )
if [ ! -z "${pythonv3bin}" ]
then
  pythonv3=$pythonv3bin
elif [ ! -z "${pythonv3binlocal}" ]
then
  pythonv3=$pythonv3binlocal
fi

if [ ! -z "${pythonv3}" ]
then
  $pythonv3 --version
  $pythonv3 -m venv virtual_env
  source ./virtual_env/bin/activate
  pip3 -V
  cd ./offline_packages
  echo "Starting installing packages"
  while read line; do pip3 install $line; done < file_list
  deactivate
  echo "Done, now activate virtual env: 'source ./virtual_env/bin/activate', and run 'python first_setup.py'"

else
  echo "Need python 3.6 or higher, script is checking /usr/bin/ and /usr/local/bin/python3 for python."
fi