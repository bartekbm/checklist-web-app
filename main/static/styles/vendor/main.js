var pluralize = function (word, count) {
  if (count === 1) { return word; }

  return word + 's';
};

var bulkSelectors = {
  'selectAll': '#select_all',
  'checkedItems': '.checkbox-item',
  'colheader': '.col-header',
  'selectedRow': 'warning',
  'updateScope': '#scope',
  'bulkActions': '#bulk_actions'
};


var momentjsClasses = function () {
  var $fromNow = $('.from-now');
  var $shortDate = $('.short-date');

  $fromNow.each(function (i, e) {
    (function updateTime() {
      var time = moment($(e).data('datetime'));
      $(e).text(time.fromNow());
      $(e).attr('title', time.format('MMMM Do YYYY, h:mm:ss a Z'));
      setTimeout(updateTime, 1000);
    })();
  });

  $shortDate.each(function (i, e) {
    var time = moment($(e).data('datetime'));
    $(e).text(time.format('MMM Do YYYY'));
    $(e).attr('title', time.format('MMMM Do YYYY, h:mm:ss a Z'));
  });
};

// Bulk delete items.
var bulkDelete = function () {
  var selectAll = '#select_all';
  var checkedItems = '.checkbox-item';
  var colheader = '.col-header';
  var selectedRow = 'warning';
  var updateScope = '#scope';
  var bulkActions = '#bulk_actions';

  $('body').on('change', checkedItems, function () {
    var checkedSelector = checkedItems + ':checked';
    var itemCount = $(checkedSelector).length;
    var pluralizeItem = pluralize('item', itemCount);
    var scopeOptionText = itemCount + ' selected ' + pluralizeItem;

    if ($(this).is(':checked')) {
      $(this).closest('tr').addClass(selectedRow);

      $(colheader).hide();
      $(bulkActions).show();
    }
    else {
      $(this).closest('tr').removeClass(selectedRow);

      if (itemCount === 0) {
        $(bulkActions).hide();
        $(colheader).show();
      }
    }

    $(updateScope + ' option:first').text(scopeOptionText);
  });

  $('body').on('change', selectAll, function () {
    var checkedStatus = this.checked;

    $(checkedItems).each(function () {
      $(this).prop('checked', checkedStatus);
      $(this).trigger('change');
    });
  });
};
setTimeout(function() {
    $('#flash-messages').fadeOut('fast');
}, 3000);

$(document).ready(function() {
  momentjsClasses();
  bulkDelete();
});

