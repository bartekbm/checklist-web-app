from flask_debugtoolbar import DebugToolbarExtension
from flask_sqlalchemy import SQLAlchemy
from flask_login import LoginManager
from flask_mail import Mail

debug_toolbar = DebugToolbarExtension()
db = SQLAlchemy()
mail = Mail()
login_manager = LoginManager()