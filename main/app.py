import locale

from flask import Flask, render_template
from itsdangerous import URLSafeTimedSerializer
from main.blueprints.admin.admin_blueprint import admin
from main.blueprints.checklist import checklist_templates, daily_checklist
from main.blueprints.page import page
from main.blueprints.user import user
from main.blueprints.reports.reports_view import reports
from main.blueprints.user.models import User
from main.extensions import (
    debug_toolbar,
    db,
    mail,
    login_manager
)
from config.settings import check_settings

def create_app(run=None):
    locale.setlocale(locale.LC_ALL, "pl_PL.UTF-8")

    app = Flask(__name__, instance_relative_config=True)


    error_templates(app)
    if check_settings():
        error_templates(app, fatal=check_settings())
    else:
        app.config.from_object('config.settings')

        app.logger.setLevel(app.config["LOG_LEVEL"])
    extensions(app)
    app.register_blueprint(admin)
    app.register_blueprint(page)
    app.register_blueprint(user)
    app.register_blueprint(checklist_templates)
    app.register_blueprint(daily_checklist)
    app.register_blueprint(reports)

    authentication(app, User)

    if run:
        if __name__ == '__main__':

            app.run()

    return app


def extensions(app):
    """
    Register 0 or more extensions (mutates the app passed in).

    :param app: Flask application instance
    :return: None
    """
    debug_toolbar.init_app(app)
    db.init_app(app)
    login_manager.init_app(app)
    mail.init_app(app)

    return None

def authentication(app, user_model):
    """
    Initialize the Flask-Login extension (mutates the app passed in).

    :param app: Flask application instance
    :param user_model: Model that contains the authentication information
    :type user_model: SQLAlchemy model
    :return: None
    """
    login_manager.login_view = 'user.login'

    @login_manager.user_loader
    def load_user(uid):
        user = user_model.query.get(uid)
        try:
            if user.active != 0:

                return user_model.query.get(uid)
            else:

                pass

        except AttributeError:
            return error_templates(app)
    # @login_manager.token_loader
    def load_token(token):

        duration = app.config['REMEMBER_COOKIE_DURATION'].total_seconds()
        serializer = URLSafeTimedSerializer(app.secret_key)

        data = serializer.loads(token, max_age=duration)
        user_uid = data[0]

        return user_model.query.get(user_uid)


def error_templates(app, fatal=False):
        """
        Register 0 or more custom error pages (mutates the app passed in).

        :param app: Flask application instance
        :return: None
        """

        def render_status(status):
            """
             Render a custom template for a specific status.
               Source: http://stackoverflow.com/a/30108946

             :param status: Status as a written name
             :type status: str
             :return: None
             """
            # Get the status code from the status, default to a 500 so that we
            # catch all types of errors and treat them as a 500.

            code = getattr(status, 'code', 500)
            if fatal:

                return render_template(f'errors/fatal_error.html',fatal=fatal)
            try:
                return render_template(f'errors/{code}.html'), code
            except Exception as e:
                return f"<p>Fatal error <br>>" \
                       f"App can't start, {e}</p>"

        for error in [404, 500]:

            app.errorhandler(error)(render_status)
        if fatal:
            app.errorhandler(error)(render_status)

        return None

def render_fatall():
    render_template('error/fatal_error.html')

create_app(run=True)
