from lib.flask_mailplus import send_template_message

from main.blueprints.user.models import User


def deliver_password_reset_email(user_id, reset_token):

    user = User.query.get(user_id)

    if user is None:
        return

    ctx = {'user': user, 'reset_token': reset_token}

    send_template_message(subject='Reset hasła z aplikacji checklist',
                          recipients=[user.email],
                          template='user/mail/password_reset', ctx=ctx)

    return None
