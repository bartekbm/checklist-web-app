from flask_wtf import FlaskForm
from wtforms import HiddenField, StringField, PasswordField
from wtforms.validators import DataRequired, Length, Optional, Regexp
from wtforms_alchemy import Unique
from wtforms_components import EmailField, Email

from lib.util_wtforms import ModelForm
from main.blueprints.user.models import User, db
from main.blueprints.user.validations import ensure_identity_exists, \
    ensure_existing_password_matches


class LoginForm(FlaskForm):
    next = HiddenField()
    identity = StringField('Nick lub Email',
                           [DataRequired(), Length(2, 254)])
    password = PasswordField('Hasło', [DataRequired(), Length(8, 128)])
    # remember = BooleanField('Stay signed in')


class BeginPasswordResetForm(FlaskForm):
    identity = StringField('Nick lub e-mail',
                           [DataRequired(),
                            Length(2, 254),
                            ensure_identity_exists])


class PasswordResetForm(FlaskForm):
    reset_token = HiddenField()
    password = PasswordField('Hasło', [DataRequired(), Length(8, 128)])


class SignupForm(ModelForm):
    email = EmailField(validators=[
        DataRequired(),
        Email(),
        Unique(
            User.email,
            get_session=lambda: db.session
        )
    ])
    name = StringField('Imię',
                           [DataRequired(), Length(2, 32)])

    last_name = StringField('Nazwisko',
                           [DataRequired(), Length(2, 32)])
    password = PasswordField('Hasło', [DataRequired(), Length(8, 128)])


class WelcomeForm(ModelForm):
    username_message = 'Dozwolone wyłącznie litery, liczby lub podkreślenie "_".'

    username = StringField('Nick', validators=[
        DataRequired(),
        Unique(
            User.username,
            get_session=lambda: db.session
        ),
        DataRequired(),
        Length(2, 16),
        Regexp('^\w+$', message=username_message)
    ])


class UpdateCredentials(ModelForm):
    current_password = PasswordField('Aktualne hasło',
                                     [DataRequired(),
                                      Length(8, 128),
                                      ensure_existing_password_matches])

    email = EmailField(validators=[
        Email(),
        Unique(
            User.email,
            get_session=lambda: db.session
        )
    ])
    password = PasswordField('Hasło', [Optional(), Length(8, 128)])
