import datetime
import pytz
from collections import OrderedDict
from flask import current_app
from flask_login import UserMixin
from hashlib import md5
from itsdangerous import URLSafeTimedSerializer, \
    TimedJSONWebSignatureSerializer
from sqlalchemy import or_
from werkzeug.security import generate_password_hash, check_password_hash

from lib.util_datetime import tzware_datetime
from lib.util_sqlalchemy import ResourceMixin, AwareDateTime
from main.extensions import db


class User(UserMixin, ResourceMixin, db.Model):
    ROLE = OrderedDict([
        ('member', 'Użytkownik'),
        ('admin', 'Admin')
    ])
    collation = 'utf8_general_ci'
    __tablename__ = 'users'
    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(AwareDateTime(),
                           default=tzware_datetime)
    # Authentication.
    role = db.Column(db.Enum(*ROLE, name='role_types', native_enum=False),
                     index=True, nullable=False, server_default='member')
    active = db.Column('is_active', db.Boolean(), nullable=False,
                       server_default='1')
    name = db.Column(db.String(24), unique=False, index=True)
    last_name = db.Column(db.String(24), unique=False, index=True)
    username = db.Column(db.String(24), unique=True, index=True)
    email = db.Column(db.String(255), unique=True, index=True, nullable=False,
                      server_default='')
    password = db.Column(db.String(128), nullable=False, server_default='')

    # Activity tracking.
    sign_in_count = db.Column(db.Integer, nullable=False, default=0)
    current_sign_in_on = db.Column(AwareDateTime())
    current_sign_in_ip = db.Column(db.String(45))
    last_sign_in_on = db.Column(AwareDateTime())
    last_sign_in_ip = db.Column(db.String(45))

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(User, self).__init__(**kwargs)

        self.password = User.encrypt_password(kwargs.get('password', ''))

    @classmethod
    def find_by_identity(cls, identity):

        return User.query.filter(
            (User.email == identity) | (User.username == identity)).first()

    @classmethod
    def encrypt_password(cls, plaintext_password):

        if plaintext_password:
            return generate_password_hash(plaintext_password)

        return None

    @classmethod
    def deserialize_token(cls, token):

        private_key = TimedJSONWebSignatureSerializer(
            current_app.config['SECRET_KEY'])
        try:
            decoded_payload = private_key.loads(token)

            return User.find_by_identity(decoded_payload.get('user_email'))
        except Exception:
            return None

    @classmethod
    def initialize_password_reset(cls, identity):

        u = User.find_by_identity(identity)
        reset_token = u.serialize_token()

        # This prevents circular imports.
        from main.blueprints.user.tasks import (
            deliver_password_reset_email)
        deliver_password_reset_email(u.id, reset_token)

        return u

    @classmethod
    def search(cls, query):

        if not query:
            return ''
        search_query = f'%{query}%'
        search_chain = (User.email.ilike(search_query),
                        User.username.ilike(search_query))
        return or_(*search_chain)

    @classmethod
    def is_last_admin(cls, user, new_role, new_active):

        is_changing_roles = user.role == 'admin' and new_role != 'admin'
        is_changing_active = user.active is True and new_active is None

        if is_changing_roles or is_changing_active:
            admin_count = User.query.filter(User.role == 'admin').count()
            active_count = User.query.filter(User.is_active is True).count()

            if admin_count == 1 or active_count == 1:
                if user.role != 'admin':
                    pass
                else:
                    return True

        return False

    def is_active(self):

        return self.active

    def get_auth_token(self):

        private_key = current_app.config['SECRET_KEY']

        serializer = URLSafeTimedSerializer(private_key)
        data = [str(self.id), md5(self.password.encode('utf-8')).hexdigest()]

        return serializer.dumps(data)

    def authenticated(self, with_password=True, password=''):

        if with_password:
            return check_password_hash(self.password, password)

        return True

    def serialize_token(self, expiration=3600):

        private_key = current_app.config['SECRET_KEY']

        serializer = TimedJSONWebSignatureSerializer(private_key, expiration)
        return serializer.dumps({'user_email': self.email}).decode('utf-8')

    def update_activity_tracking(self, ip_address):

        self.sign_in_count += 1

        self.last_sign_in_on = self.current_sign_in_on
        self.last_sign_in_ip = self.current_sign_in_ip

        self.current_sign_in_on = datetime.datetime.now(pytz.utc)
        self.current_sign_in_ip = ip_address

        return self.save()


class Group(db.Model, ResourceMixin):
    __tablename__ = 'roles'
    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(AwareDateTime(),
                           default=tzware_datetime)
    name = db.Column(db.String(120), nullable=True)

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(Group, self).__init__(**kwargs)

    @classmethod
    def search(cls, query):


        if not query:
            return ''
        search_query = f'%{query}%'
        search_chain = (Group.name.ilike(search_query))
        return (search_chain)

    @classmethod
    def create(cls, params):


        group = Group(**params)

        db.session.add(group)
        db.session.commit()

        return True

    @classmethod
    def bulk_delete(cls, ids):

        delete_count = 0

        for id in ids:
            group = Group.query.get(id)

            if Group is None:
                continue

            group.delete()
            delete_count += 1

        return delete_count

    @classmethod
    def find_by_name(cls, id):

        query = Group.query.filter(Group.id == id).first()
        return query


class UserRole(db.Model):
    __tablename__ = 'user_roles'
    id = db.Column(db.Integer, primary_key=True)
    UserID = db.Column(db.Integer, db.ForeignKey('users.id', ondelete='CASCADE'),
                       nullable=False)
    RoleID = db.Column(db.Integer, db.ForeignKey('roles.id', ondelete='CASCADE'),
                       nullable=False)

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(UserRole, self).__init__(**kwargs)

    @classmethod
    def create(cls, params):
        roles = UserRole(**params)

        db.session.add(roles)
        db.session.commit()

    @staticmethod
    def roles_delete(id, roles):
        to_delete = UserRole.query.filter(UserRole.UserID == id, UserRole.RoleID == roles)
        to_delete.delete()
        db.session.commit()

    @staticmethod
    def roles_add(params):
        UserRole.create(params)
