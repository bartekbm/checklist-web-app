from flask import (
    redirect,
    request,
    flash,
    url_for,
    render_template)
from flask_login import login_required
from .admin_blueprint import admin
from main.blueprints.admin.forms import (
    SearchForm,
    BulkDeleteForm,
    GroupForm
)
from sqlalchemy import text
from main.blueprints.user.decorators import role_required
from main.blueprints.user.models import Group


@admin.before_request
@login_required
@role_required('admin')
def before_request():

    pass


# Groups -----------------------------------------------------------------------
@admin.route('/groups', defaults={'page': 1})
@admin.route('/groups/page/<int:page>')
def groups(page):
    search_form = SearchForm()
    bulk_form = BulkDeleteForm()

    sort_by = Group.sort_by(request.args.get('sort', 'created_on'),
                            request.args.get('direction', 'desc'))
    order_values = f'{sort_by[0]} {sort_by[1]}'
    paginated_groups = Group.query \
        .filter(Group.search(request.args.get("q", ''))) \
        .order_by(text(order_values)) \
        .paginate(page, 7, True)

    return render_template('admin/groups/index.html',
                           form=search_form, bulk_form=bulk_form,
                           groups=paginated_groups)


@admin.route('/groups/edit/<int:id>', methods=['GET', 'POST'])
def groups_edit(id):
    group = Group.query.get(id)
    form = GroupForm(obj=group)

    if form.validate_on_submit():

        form.populate_obj(group)

        if not group.name:
            group.name = None

        group.save()

        flash('Grupa zapisana.', 'success')
        return redirect(url_for('admin.groups'))

    return render_template('admin/groups/edit.html', form=form, group=group)


@admin.route('/groups/new', methods=['GET', 'POST'])
def groups_new():
    group = Group()
    form = GroupForm(obj=group)

    if form.validate_on_submit():
        form.populate_obj(group)

        params = {
            'name': group.name
        }

        if Group.create(params):
            flash('Grupa zapisana pomyślnie.', 'success')
            return redirect(url_for('admin.groups'))

    return render_template('admin/groups/new.html', form=form, group=group)


@admin.route('/group/bulk_delete', methods=['POST'])
def Group_bulk_delete():
    form = BulkDeleteForm()

    if form.validate_on_submit():
        ids = Group.get_bulk_action_ids(request.form.get('scope'),
                                        request.form.getlist('bulk_ids'),
                                        query=request.args.get('q', ''))

        delete_count = Group.bulk_delete(ids)

        flash(f'{delete_count} grup zostało usuniętych.','success')
    else:
        flash('Grupy nie zostali usunięci, coś poszło nie tak.', 'error')

    return redirect(url_for('admin.groups'))
