from collections import OrderedDict
from flask_wtf import FlaskForm
from wtforms import SelectField, StringField, BooleanField, SelectMultipleField, widgets
from wtforms.validators import DataRequired, Length, Optional, Regexp
from wtforms_alchemy import Unique

from lib.util_wtforms import ModelForm, choices_from_dict
from main.blueprints.user.models import db, User, Group


class SearchForm(FlaskForm):
    q = StringField('Wyszukiwana fraza', [Optional(), Length(1, 256)])


class BulkDeleteForm(FlaskForm):
    SCOPE = OrderedDict([
        ('all_selected_items', 'All selected items'),
        ('all_search_results', 'All search results')
    ])

    scope = SelectField('Uprawniena', [DataRequired()],
                        choices=choices_from_dict(SCOPE, prepend_blank=False))


class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

    def process_data(self, value):
        return super(MultiCheckboxField, self).process_data(value)


class RolesForm(ModelForm):
    user_roles = MultiCheckboxField()


class UserForm(ModelForm):
    username_message = 'max 10 znaków'

    username = StringField('Nick', validators=[
        Unique(
            User.username,
            get_session=lambda: db.session
        ),
        Optional(),
        Length(1, 10),
        Regexp('^\w+$', message=username_message)
    ])
    name = StringField('Imię', validators=[
        Optional(),
        Length(1, 32)
    ])
    last_name = StringField('Nazwisko', validators=[
        Optional(),
        Length(1, 32)
    ])

    role = SelectField('Uprawnienia', [DataRequired()],
                       choices=choices_from_dict(User.ROLE,
                                                 prepend_blank=False))
    active = BooleanField('Tak, pozwalaj na logowanie')


class GroupForm(ModelForm):
    name = StringField(validators=[
        Unique(
            Group.name,
            get_session=lambda: db.session
        ),
        Length(1, 120),
    ])







