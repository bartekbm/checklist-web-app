from sqlalchemy import func
from sqlalchemy import and_
from main.blueprints.user.models import db, User, Group, UserRole


class Dashboard:
    @classmethod
    def group_and_count_users(cls):

        return Dashboard._group_and_count(User, User.role)

    @classmethod
    def _group_and_count(cls, model, field):

        count = func.count(field)
        query = db.session.query(count, field).group_by(field).all()

        results = {
            'query': query,
            'total': model.query.count()
        }

        return results

class Roles:

    @classmethod
    def get_all_roles(cls, id):
        return Roles._get_all_roles(id)

    @classmethod
    def _get_all_roles(cls, id):

        query = db.session.query(Group.id, Group.name, UserRole.UserID)\
            .outerjoin(UserRole, and_(Group.id == UserRole.RoleID, UserRole.UserID == f'{id}')).order_by(Group.name)

        return query

    @classmethod
    def get_specify_role(cls,name):
        query=db.session.query(Group.id).filter(Group.name == f'{name}')

        return query

    @classmethod
    def get_specify_role_by_user(cls, id):
        query = db.session.query(Group.id, Group.name)\
            .join(UserRole, and_(Group.id == UserRole.RoleID, UserRole.UserID == f'{id}')).order_by(Group.name)

        return query

class Roleshandling:
    @staticmethod
    def process_result(result,roles,id):

            to_skip = []
            result_list = []
            for r in roles:
                if not result and (r.UserID is not None):
                    result_list.append(('to_delete',r.name))
                for a in result:
                    if a == r.name and (r.UserID == id):
                        to_skip.append(a)
                for i in result:
                    if i == r.name and (r.UserID is None):
                        result_list.append(('to_add',r.name))
                    elif i != r.name and (r.UserID is not None):
                        if r.name in to_skip:
                            pass
                        else:
                            result_list.append(('to_delete',r.name))
            result_list = list(dict.fromkeys(result_list))
            for a in result_list:
                if a[0] == 'to_delete':
                    b = [b.id for b in Roles.get_specify_role(a[1])]
                    UserRole.roles_delete(id, b[0])

                else:
                    params = {
                        'UserID': id,
                        'RoleID': [b.id for b in Roles.get_specify_role(a[1])][0]
                    }
                    UserRole.roles_add(params)


