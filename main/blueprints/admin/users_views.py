from flask import (
    redirect,
    request,
    flash,
    url_for,
    render_template)
from flask_login import login_required, current_user
from sqlalchemy import text

from main.blueprints.admin.forms import (
    SearchForm,
    BulkDeleteForm,
    UserForm,
    RolesForm
)
from main.blueprints.admin.models import Dashboard, Roles, Roleshandling
from main.blueprints.user.decorators import role_required
from main.blueprints.user.models import User
from .admin_blueprint import admin


@admin.before_request
@login_required
@role_required('admin')
def before_request():

    pass


# Dashboard -------------------------------------------------------------------
@admin.route('')
def dashboard():
    group_and_count_users = Dashboard.group_and_count_users()

    return render_template('admin/page/dashboard.html',
                           group_and_count_users=group_and_count_users)


# Users -----------------------------------------------------------------------
@admin.route('/users', defaults={'page': 1})
@admin.route('/users/page/<int:page>')
def users(page):
    search_form = SearchForm()
    bulk_form = BulkDeleteForm()

    sort_by = User.sort_by(request.args.get('sort', 'created_on'),
                           request.args.get('direction', 'desc'))
    order_values = f'{sort_by[0]} {sort_by[1]}'
    paginated_users = User.query \
        .filter(User.search(request.args.get("q",''))) \
        .order_by(User.role.asc(), text(order_values)) \
        .paginate(page, 7, True)

    return render_template('admin/user/index.html',
                           form=search_form, bulk_form=bulk_form,
                           users=paginated_users)

class ChoiceObj:
    def __init__(self, name, choices):
        # this is needed so that BaseForm.process will accept the object for the named form,
        # and eventually it will end up in SelectMultipleField.process_data and get assigned
        # to .data
        setattr(self, name, choices)


@admin.route('/users/edit/<int:id>', methods=['GET', 'POST'])
def users_edit(id):
    user = User.query.get(id)
    roles = Roles.get_all_roles(id)
    form = UserForm(obj=user)

    selectedchoices = ChoiceObj('user_roles', [role.name for role in roles if role.UserID is not None])
    form_roles = RolesForm(obj=selectedchoices)

    form_roles.user_roles.choices = [(role.name,role.name) for role in roles]

    if form.validate_on_submit():
        if User.is_last_admin(user,
                              request.form.get('role'),
                              request.form.get('active')):
            flash('Jesteś ostatnim adminem, nie można tego wykonać.', 'error')
            return redirect(url_for('admin.users'))

        form.populate_obj(user)

        if not user.username:
            user.username = None

        user.save()

        result = request.form.getlist('user_roles')

        Roleshandling.process_result(result, roles, id)


        flash('Użytkownik zapisany.', 'success')
        return redirect(url_for('admin.users'))

    return render_template('admin/user/edit.html', form=form, user=user, roles=roles,form_roles=form_roles)


@admin.route('/users/bulk_delete', methods=['POST'])
def users_bulk_delete():
    form = BulkDeleteForm()

    if form.validate_on_submit():
        ids = User.get_bulk_action_ids(request.form.get('scope'),
                                       request.form.getlist('bulk_ids'),
                                       omit_ids=[current_user.id],
                                       query=request.args.get('q',''))

        delete_count = User.bulk_delete(ids)

        flash(f'{delete_count} użytkowników zostało usuniętych.','success')
    else:
        flash('Użytkownicy nie zostali usunięci, coś poszło nie tak.', 'error')

    return redirect(url_for('admin.users'))
