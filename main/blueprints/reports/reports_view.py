from flask import (
    Blueprint,
    render_template,
    request)
import os, io
from flask_login import login_required, current_user
from main.blueprints.reports.forms import DateForm, UserList, GroupList, TemplateList
from main.blueprints.reports.models.reports import Reports
from main.blueprints.user.models import User, Group
from main.blueprints.checklist.models.checklist_template import ChecklistTemplate
from flask import send_file, flash
import uuid
reports = Blueprint('reports', __name__, template_folder='templates')


@reports.before_request
@login_required
def before_request():

    pass


#generate random file name
def file_namer(string_length=10):
    random = str(uuid.uuid4())  # Convert UUID format to a Python string.
    random = random.upper()  # Make all characters uppercase.
    random = random.replace("-", "")  # Remove the UUID '-'.
    return random[0:string_length]  # Return the random string.


@reports.route('/reports', methods=['GET', 'POST'])
def reports_view():
    form = DateForm()
    reports = Reports()


    # user report
    if current_user.role != 'admin':

        user_data = User.query.order_by('last_name').filter(User.id == current_user.id)
    else:
        user_data = User.query.order_by('last_name').filter(User.is_active(User))
    try:
        user_info = [(user.id, user.last_name + ' ' + user.name + ' ' + user.username) for user in user_data]
    except TypeError:
        user_info = [(user.id, user.email) for user in user_data]
    user_form = UserList()
    user_form.List_form_user.choices = user_info

    # group report
    all_groups = Group.query.order_by('name')
    group_info = [(group.id, group.name) for group in all_groups]
    group_form = GroupList()
    group_form.List_form_group.choices = group_info

    # templates report
    all_templates = ChecklistTemplate.query.order_by('name')
    template_info = [(template.id, template.name) for template in all_templates]
    template_form = TemplateList()
    template_form.List_form_template.choices = template_info

    if group_form.is_submitted() and request.form['action'] == 'Group':

        group = request.form.get('List_form_group')

        get_users = reports.get_user_data_from_group(group)

        new_report = reports.generate_reports(form.start.raw_data, form.end.raw_data, get_users)
        new_file = file_namer()
        report_info = reports.report_to_xls(new_report, new_file)
        if not report_info:
            flash('Brak danych w podanym przedziale', 'warning')
            return render_template('index.html', form=form, user_form=user_form, group_form=group_form,
                                   template_form=template_form)

        result = send_attachment(report_info, new_file, type_name='Group')

        return result


    if user_form.is_submitted() and request.form['action'] == 'User':

        # User
        user = request.form.get('List_form_user')

        new_report = reports.generate_reports(form.start.raw_data, form.end.raw_data, user)
        new_file = file_namer()
        report_info = reports.report_to_xls(new_report, new_file)
        if not report_info:
            flash('Brak danych w podanym przedziale', 'warning')
            return render_template('index.html', form=form, user_form=user_form, group_form=group_form,
                                   template_form=template_form)

        result = send_attachment(report_info, new_file, type_name='User')

        return result



    if template_form.is_submitted() and request.form['action'] == 'Template':
        get_temaplte = request.form.get('List_form_template')
        new_report = reports.generate_reports(form.start.raw_data, form.end.raw_data, get_temaplte, template=True)
        new_file = file_namer()
        report_info = reports.report_to_xls(new_report, new_file)
        if not report_info:
            flash('Brak danych w podanym przedziale', 'warning')
            return render_template('index.html', form=form, user_form=user_form, group_form=group_form,
                                   template_form=template_form)

        result = send_attachment(report_info, new_file, type_name='Template')

        return result

    return render_template('index.html', form=form, user_form=user_form, group_form=group_form,
                           template_form=template_form)


def send_attachment(name_vars, file, type_name):
    name_vars = name_vars.replace(':', '_').replace(' ', 'T').replace('.', '')
    return_data = io.BytesIO()
    with open(file, 'rb') as fo:
        return_data.write(fo.read())
    return_data.seek(0)
    os.remove(file)
    result = send_file(return_data,
                       mimetype="application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                       attachment_filename=f'{type_name}_report_D{name_vars}.xlsx',
                       as_attachment=True,
                       conditional=False)

    return result
