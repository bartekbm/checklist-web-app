from main.blueprints.user.models import db, User, UserRole
from main.blueprints.checklist.models.task import Task, TaskTemplate
from main.blueprints.checklist.models.checklist import Checklist, ChecklistTemplate

from sqlalchemy import func
import collections
import xlsxwriter
from datetime import datetime



class Reports:

    def user_reports(self, date_from, date_to, user_id, tasks):

        query = db.session.query(Task.TaskTemplateID.label('id'), Task.start.label('start'), Task.stop.label('stop'),
                                 Task.length.label('dlugosc'),
                                 Task.finished_on.label('zakonczono'), TaskTemplate.name.label('NazwaZadania'),
                                 Checklist.name.label('NazwaChecklisty')) \
            .filter(User.id == Task.user_id) \
            .filter(TaskTemplate.id == Task.TaskTemplateID) \
            .filter(Task.ChecklistId == Checklist.id) \
            .filter(Task.finished_on.between(date_from, date_to)) \
            .filter(Task.user_id == user_id).order_by('zakonczono')


        query2 = db.session.query(Task.TaskTemplateID.label('id'),
                                  TaskTemplate.name.label('NazwaWzoruZadnia'),
                                  ChecklistTemplate.name.label('NazwaWzoruChecklisty'),
                                  func.sum(func.time_to_sec(Task.length)).label('SumaCzasuNaZadanieSekundy'),
                                  func.sum(func.time_to_sec(Task.length) / 60).label('SumaCzasuNaZadanieMinuty')) \
            .filter(User.id == Task.user_id) \
            .filter(TaskTemplate.id == Task.TaskTemplateID) \
            .filter(Task.ChecklistId == Checklist.id) \
            .filter(TaskTemplate.checklists_templates_id == ChecklistTemplate.id) \
            .filter(Task.finished_on.between(date_from, date_to)) \
            .filter(Task.user_id == user_id).group_by('id')

        if tasks:
            query = query.filter(Task.id.in_(tasks))
            query2 = query2.filter(Task.id.in_(tasks))

        additional_data = db.session.query(User).filter(User.id == user_id)

        return query, query2, additional_data

    def get_user_data_from_group(self, group):
        query = db.session.query(UserRole.UserID).filter(UserRole.RoleID == group)
        return [user_from_group.UserID for user_from_group in query]

    def get_users_from_template(self, date_from, date_to, data_users):
        query = db.session.query(Checklist.id).filter(Checklist.ChecklistTemplateID == data_users)
        ids = [template.id for template in query]
        get_task = db.session.query(Task).filter(Task.ChecklistId.in_(ids)) \
            .filter(Task.finished_on.between(date_from, date_to))

        return [task.id for task in get_task], [task.user_id for task in get_task]

    def generate_reports(self, date_from, date_to, data_users, template=False):
        tasks = []
        if template:
            tasks, users = self.get_users_from_template(date_from, date_to, data_users)
            data_users = list(dict.fromkeys(users))


        dict_user = {
            'detail_user_reports': '',
            'user_report': '',
            'additional_data': ''
        }
        all_users = []

        for user in data_users:
            detail_user_reports, user_report, additional_data = self.user_reports(date_from, date_to, user, tasks)
            try:
                dict_user['detail_user_reports'] = self.prepare_report(detail_user_reports[0].keys(),
                                                                       detail_user_reports)
                dict_user['user_report'] = self.prepare_report(user_report[0].keys(), user_report)
                dict_user['additional_data'] = [(data.name, data.last_name, data.username) for data in additional_data]
            except(ValueError, IndexError):
                dict_user['detail_user_reports'] = 'Brak danych'
                dict_user['user_report'] = 'Brak danych'
                dict_user['additional_data'] = [(data.name, data.last_name, data.username) for data in additional_data]
            all_users.append(dict_user.copy())

        return all_users

    def prepare_report(self, column_name, result_of_query):

        column_name.remove('id')
        column = {}
        data_to_save = collections.defaultdict(list)
        for name in column_name:
            column[name] = ''
        for data in result_of_query:
            for key_name in column.keys():
                data_to_save[key_name].append(eval(f'data.{key_name}'))
        return data_to_save

    def report_to_xls(self, report_dict, file):

        #if all empty, return that no existing data
        max = len(report_dict)
        count = 0
        for report in report_dict:
            if report['user_report'] == 'Brak danych':
                count += 1
        if max == count:
            return False
        workbook = xlsxwriter.Workbook(file)

        for report in report_dict:
            col = 0
            try:
                # max worksheet name is 31 len
                worksheet = workbook.add_worksheet(str(report['additional_data'][0]))
            except xlsxwriter.exceptions.InvalidWorksheetName:
                if len(str(report['additional_data'][0])) >= 31:
                    truncate = str(report['additional_data'][0])
                    truncate = truncate[:29]
                    add = "')"
                    truncate = truncate + add
                    worksheet = workbook.add_worksheet(truncate)

            worksheet.write(0, 0, str(report['additional_data'][0]) + ' wygenerowano: ' + str(datetime.now()))
            worksheet.write(2, 0, 'Raport ogólny')

            try:
                for key, value in report['user_report'].items():
                    worksheet.write(3, col, key)
                    worksheet.write_column(4, col, value)
                    col += 1
                    row = len(value) + 5
            except AttributeError:
                key = 'Brak Danych w podanym przedziale'
                worksheet.write(3, col, key)
                row = 4

            worksheet.write(row, 0, 'Raport szczegółowy')

            date_format = ''
            col = 0
            try:
                for key, value in report['detail_user_reports'].items():
                    if key == 'start' or key == 'stop' or key == 'dlugosc':
                        date_format = workbook.add_format({'num_format': 'hh:mm:ss'})
                    if key == 'zakonczono':
                        date_format = workbook.add_format({'num_format': 'yyyy-mm-dd hh:mm:ss'})

                    worksheet.write(row + 1, col, key)
                    worksheet.write_column(row + 2, col, value, date_format)
                    col += 1
            except AttributeError:
                key = 'Brak Danych w podanym przedziale'
                worksheet.write(row + 1, col, key)

        workbook.close()
        return str(datetime.now())
