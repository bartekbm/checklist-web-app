from flask_wtf import FlaskForm
from wtforms import DateTimeField, SelectField
from wtforms.validators import DataRequired
from lib.util_wtforms import ModelForm

class DateForm(FlaskForm):
    start = DateTimeField('Start', [DataRequired()], format='%Y-%m-%d %H:%M:%S')

    end = DateTimeField('End', [DataRequired()], format='%Y-%m-%d %H:%M')


class UserList(ModelForm):
    List_form_user = SelectField("")


class GroupList(ModelForm):
    List_form_group = SelectField("")


class TemplateList(ModelForm):
    List_form_template = SelectField("")