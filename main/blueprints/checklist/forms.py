from wtforms import SelectField, StringField, validators, TimeField
from wtforms.validators import Length

from lib.util_wtforms import ModelForm



class ChecklistTemplateForm(ModelForm):

    name = StringField(u'Nazwa', validators=[Length(min=1,max=24), validators.required()])

    RoleID = SelectField("Wybierz grupę:", coerce=int)

class TaskForm(ModelForm):
    name = StringField(u'Nazwa', validators=[Length(min=1,max=60), validators.required()])
    a_when = TimeField(u'Kiedy', validators=[validators.required()])
    description = StringField(u'Opis', validators=[validators.optional()])
    who = StringField(u'Kto', validators=[Length(max=7),validators.optional()])
    #daily_task = BooleanField('Zadanie codzienne') #project not prepared for calendar tasks



class ChecklistForm(ModelForm):


    List_form = SelectField("")
