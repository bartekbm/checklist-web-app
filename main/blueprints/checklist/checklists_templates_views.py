from flask import (
    Blueprint,
    redirect,
    request,
    flash,
    url_for,
    render_template)
from flask_login import login_required, current_user
from flask_sqlalchemy import BaseQuery
from sqlalchemy import text

from main.blueprints.admin.forms import (
    SearchForm,
    BulkDeleteForm)
from main.blueprints.admin.models import Roles
from main.blueprints.checklist.forms import ChecklistTemplateForm, TaskForm
from main.blueprints.checklist.models.checklist_template import ChecklistTemplate
from main.blueprints.checklist.models.task_template import TaskTemplate
from main.blueprints.user.models import Group
from main.blueprints.checklist.models.task import Task
checklist_templates = Blueprint('checklist_template', __name__, template_folder='templates')


def paginate(sa_query, page, per_page=10, error_out=True):
    sa_query.__class__ = BaseQuery
    # We can now use BaseQuery methods like .paginate on our SA query
    return sa_query.paginate(page, per_page, error_out)


@checklist_templates.before_request
@login_required
def before_request():

    pass


@checklist_templates.route('/checklist_template', defaults={'page': 1})
@checklist_templates.route('/checklist_template/<int:page>')
def checklist_template(page):
    roles = Roles.get_specify_role_by_user(current_user.id)
    checklist_with_roles = ChecklistTemplate.get_by_user_group([role.id for role in roles])
    search_form = SearchForm()
    bulk_form = BulkDeleteForm()
    sort_by = ChecklistTemplate.sort_by(request.args.get('sort', 'created_on'),
                                        request.args.get('direction', 'desc'))
    order_values = f'{sort_by[0]} {sort_by[1]}'
    paginated_checklist = checklist_with_roles \
        .filter(ChecklistTemplate.search(request.args.get("q", ''))) \
        .order_by(text(order_values))

    paginated_checklist = paginate(paginated_checklist, page)

    return render_template('checklist_template/index.html',
                           form=search_form, bulk_form=bulk_form,
                           paginated_checklist=paginated_checklist)


@checklist_templates.route('/checklist_template/edit_tasks/<int:checklists_templates_id>/page/<int:page>',
                           methods=['GET', 'POST'])
def task(checklists_templates_id, page):
    search_form = SearchForm()
    bulk_form = BulkDeleteForm()
    pre_task = TaskTemplate.get_by_checklist_id(checklists_templates_id)
    sort_by = TaskTemplate.sort_by(request.args.get('sort', 'a_when'),
                                   request.args.get('direction', 'asc'))
    order_values = f'{sort_by[0]} {sort_by[1]}'
    paginated_task = pre_task \
        .filter(TaskTemplate.search(request.args.get("q", ''))) \
        .order_by(text(order_values))
    paginated_task = paginate(paginated_task, page)
    secure = ChecklistTemplate.check_if_authorised(checklists_templates_id)
    if secure:
        return redirect(url_for('checklist_template.checklist_template'))
    else:
        return render_template('task_template/index.html', form=search_form, bulk_form=bulk_form, tasks=paginated_task,
                               id=checklists_templates_id, page=page)


@checklist_templates.route('/checklist_template/edit_tasks/new_task/<int:checklists_templates_id>',
                           defaults={'exist_checklist_id': 0}, methods=['GET', 'POST'])
@checklist_templates.route('/checklist_template/edit_tasks/new_task/<int:checklists_templates_id>/<int:exist_checklist_id>',
                           methods=['GET', 'POST'])
def new_task(checklists_templates_id, exist_checklist_id):

    new_task = TaskTemplate()

    form = TaskForm(obj=new_task)

    if form.validate_on_submit():
        form.populate_obj(new_task)
        if new_task.who == '':
            new_task.who = None
        params = {
            'name': new_task.name,
            'a_when': new_task.a_when,
            'description': new_task.description,
            'who': new_task.who,
            'checklists_templates_id': checklists_templates_id
        }
        create = new_task.create(params)
        if exist_checklist_id != 0 and create:
            flash('Nowe zadanie stworzone, dodaję do aktualnej checklisty.', 'success')
            add_task = Task()
            task_params = {
                'TaskTemplateID': create,
                'ChecklistId': exist_checklist_id
            }
            add_task.create(task_params)
            return redirect(url_for('daily_checklist.checklist_do', id=exist_checklist_id, page=1))
        if create:
            flash('Nowe zadanie stworzone.', 'success')
            return redirect(url_for('checklist_template.task', page=1, checklists_templates_id=checklists_templates_id,
                                    exist_checklist_id=exist_checklist_id))
    secure = ChecklistTemplate.check_if_authorised(checklists_templates_id)
    if secure:
        return redirect(url_for('checklist_template.checklist_template'))
    else:
        return render_template('task_template/new.html', form=form, new_task=new_task, id=checklists_templates_id,
                               exist_checklist_id=exist_checklist_id)


@checklist_templates.route('/checklist_template/edit_task/edit/<int:id>/<int:checklists_templates_id>',
                           methods=['GET', 'POST'])
def edit_task(id, checklists_templates_id):
    task_edit = TaskTemplate.query.get(id)
    form = TaskForm(task_edit)
    if form.validate_on_submit():
        form.populate_obj(task_edit)
        task_edit.save()
        flash('Zadanie zapisane.', 'success')
        return redirect(url_for('checklist_template.task', page=1, checklists_templates_id=checklists_templates_id))
    secure = ChecklistTemplate.check_if_authorised(checklists_templates_id)
    if secure:
        return redirect(url_for('checklist_template.checklist_template'))
    else:
        return render_template('task_template/edit.html', form=form, task_edit=task_edit,
                               id=checklists_templates_id)


@checklist_templates.route('/checklist_template/bulk_delete_task/<int:checklists_templates_id>', methods=['POST'])
def bulk_delete_task(checklists_templates_id):
    form = BulkDeleteForm()

    if form.validate_on_submit():
        ids = TaskTemplate.get_bulk_action_ids(request.form.get('scope'), request.form.getlist('bulk_ids'),
                                               checklists_templates_id=checklists_templates_id,
                                               query=request.args.get('q', ''))

        delete_count = TaskTemplate.bulk_delete(ids)

        flash(f'{delete_count} zadań usuniętych.',
              'success')
    else:
        flash('Żadne zadanie nie zostało usunięte, coś poszło nie tak.', 'error')

    return redirect(url_for('checklist_template.task', page=1, checklists_templates_id=checklists_templates_id))


@checklist_templates.route('/checklist_template/edit/<int:id>', methods=['GET', 'POST'])
def checklist_template_edit(id):
    roles = Roles.get_specify_role_by_user(current_user.id)
    checklist_template = ChecklistTemplate.query.get(id)
    secure = ChecklistTemplate.check_if_authorised(id)
    group = Group.find_by_name(checklist_template.RoleID)
    form = ChecklistTemplateForm(checklist_template)
    roles_list = [(role.id, role.name) for role in roles]
    form.RoleID.choices = roles_list

    if form.validate_on_submit():

        form.populate_obj(checklist_template)

        if not checklist_template.name:
            checklist_template.name = None

        checklist_template.save()

        flash('Wzór zapisany.', 'success')
        return redirect(url_for('checklist_template.checklist_template'))
    if secure:
        return redirect(url_for('checklist_template.checklist_template'))
    else:
        return render_template('checklist_template/edit.html', form=form, checklist_template=checklist_template,
                               group_name=group)


@checklist_templates.route('/checklist_template/new', methods=['GET', 'POST'])
def checklist_template_new():
    checklist_template = ChecklistTemplate()
    roles = Roles.get_specify_role_by_user(current_user.id)

    roles_list = [(role.id, role.name) for role in roles]
    form = ChecklistTemplateForm(obj=checklist_template)
    form.RoleID.choices = roles_list
    if form.validate_on_submit():
        form.populate_obj(checklist_template)

        params = {
            'name': checklist_template.name,
            'RoleID': request.form.get('RoleID')
        }

        if ChecklistTemplate.create(params):
            flash('Nowy wzór stworzony.', 'success')
            return redirect(url_for('checklist_template.checklist_template'))

    return render_template('checklist_template/new.html', form=form, checklist_template=checklist_template)


@checklist_templates.route('/checklist_template/bulk_delete', methods=['POST'])
def checklist_template_bulk_delete():
    form = BulkDeleteForm()
    roles = Roles.get_specify_role_by_user(current_user.id)
    roles = [role.id for role in roles]

    if form.validate_on_submit():
        ids = ChecklistTemplate.get_bulk_action_ids(request.form.get('scope'),
                                                    request.form.getlist('bulk_ids'),
                                                    roles=roles,
                                                    query=request.args.get('q', ''))
        print(ids)
        delete_count = ChecklistTemplate.bulk_delete(ids)

        flash(f'{delete_count} wozrów usunięto.',
              'success')
    else:
        flash('Nie usunięto żadnego wzoru, coś poszło nie tak....', 'error')

    return redirect(url_for('checklist_template.checklist_template'))
