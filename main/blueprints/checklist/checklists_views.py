import ast
import datetime
from datetime import timedelta
from flask import (
    flash,
    Blueprint,
    request,
    url_for,
    redirect,
    render_template)
from flask_login import login_required, current_user
from sqlalchemy import text
from main.blueprints.user.models import db
from lib.util_datetime import checklist_date
from main.blueprints.admin.forms import (
    SearchForm,
    BulkDeleteForm)
from main.blueprints.admin.models import Roles
from main.blueprints.checklist.checklists_templates_views import paginate
from main.blueprints.checklist.forms import ChecklistForm
from main.blueprints.checklist.models.checklist import Checklist
from main.blueprints.checklist.models.checklist_template import ChecklistTemplate
from main.blueprints.checklist.models.task import Task
from main.blueprints.checklist.models.task_template import TaskTemplate
from lib.util_datetime import tzware_datetime
daily_checklist = Blueprint('daily_checklist', __name__, template_folder='templates')


@daily_checklist.before_request
@login_required
def before_request():

    pass


@daily_checklist.route('/checklist', defaults={'page': 1})
@daily_checklist.route('/checklist/<int:page>')
def checklist(page):
    roles = Roles.get_specify_role_by_user(current_user.id)
    roles = [role.id for role in roles]

    checklist_template_with_roles = ChecklistTemplate.get_by_user_group(roles)
    form_from_template = ChecklistForm(checklist_template_with_roles)
    checklist_list = [(checklist.id, checklist.name) for checklist in checklist_template_with_roles]
    form_from_template.List_form.choices = checklist_list

    checklist_with_roles = Checklist.get_by_user_group(roles)
    search_form = SearchForm()
    bulk_form = BulkDeleteForm()
    sort_by = Checklist.sort_by(request.args.get('sort', 'created_on'),
                                request.args.get('direction', 'desc'))

    order_values = f'{sort_by[0]} {sort_by[1]}'

    paginated_checklist = checklist_with_roles.filter(Checklist.search(request.args.get("q", ''))) \
        .order_by(text(order_values))
    paginated_checklist = paginate(paginated_checklist, page)

    return render_template('checklist/index.html',
                           form=search_form, bulk_form=bulk_form,
                           paginated_checklist=paginated_checklist, form_from_template=form_from_template)


@daily_checklist.route('/checklist/get_the_job_done/<int:id>/page/<int:page>',
                       methods=['GET', 'POST'])
def checklist_do(id, page):
    search_form = SearchForm()
    tasks = Task.get_by_checklist_id(id)
    get_checklist_template = db.session.query(Checklist.ChecklistTemplateID).filter(Checklist.id == id).first()
    get_checklist_template = [id for id in get_checklist_template][0]

    sort_by = Task.sort_by(request.args.get('sort', 'a_when'),
                           request.args.get('direction', 'asc'))

    order_values = f'{sort_by[0]} {sort_by[1]}'

    paginated_task = tasks.filter(Task.search(request.args.get("q", ''))).order_by(text(order_values))
    paginated_task = paginate(paginated_task, page)

    secure = Checklist.check_if_authorised(id)
    if secure:
        return redirect(url_for('daily_checklist.checklist'))
    else:
        return render_template('task/index.html', tasks=paginated_task, form=search_form, id=id, page=page,
                               get_checklist_template=get_checklist_template)


@daily_checklist.route('/checklist_template/new/', methods=['GET', 'POST'])
def checklist_new():
    get_template_resultas_id = request.form.get('List_form')

    if get_template_resultas_id is None:
        flash('Nie ma żadnych wzorów, stwórz chociaż jeden', 'error')
        return redirect(url_for('daily_checklist.checklist'))
    else:
        checklist = Checklist.get_template_id(get_template_resultas_id)
        name, roleid = checklist

        checklist_params = {
            'name': checklist_date() + name[0],
            'RoleID': roleid[0],
            'ChecklistTemplateID': get_template_resultas_id

        }
        id_added_checklist = Checklist.create(checklist_params)
        populate_tasks = TaskTemplate.get_by_checklist_id(get_template_resultas_id)
        for task in populate_tasks:
            task_params = {
                'TaskTemplateID': task.id,
                'ChecklistId': id_added_checklist
            }
            Task.create(task_params)

        flash('Nowa checklista stworzona.', 'success')
        return redirect(url_for('daily_checklist.checklist'))


@daily_checklist.route('/checklist/bulk_delete', methods=['POST'])
def checklist_bulk_delete():
    form = BulkDeleteForm()
    roles = Roles.get_specify_role_by_user(current_user.id)
    roles = [role.id for role in roles]

    if form.validate_on_submit():
        ids = Checklist.get_bulk_action_ids(request.form.get('scope'),
                                            request.form.getlist('bulk_ids'),
                                            roles=roles,
                                            query=request.args.get('q', ''))

        delete_count = Checklist.bulk_delete(ids)

        flash(f'{delete_count} checklist usunięto.',
              'success')
    else:
        flash('Nie usunięto żadnej checklisty, coś poszło nie tak....', 'error')

    return redirect(url_for('daily_checklist.checklist'))


@daily_checklist.route('/checklist/event/<data>', methods=['POST'])
def event(data):
    get_session = db.session
    initials = str([a.username for a in get_session][0])
    id_user = int([a.id for a in get_session][0])

    start = request.form.get('start_task')
    stop = request.form.get('end_task')
    now = datetime.datetime.now()

    now = now.strftime("%H:%M:%S")
    if start:
        task = Task.query.get(start)
        task.user_id = id_user
        task.start = now
        task.save()
    if stop:
        task = Task.query.get(stop)
        task.stop = now
        if task.start is None:
            task.start = datetime.datetime.strptime(str(task.stop), "%H:%M:%S") + timedelta(seconds=-1)
        task.save()

        converted_stop = datetime.datetime.strptime(str(task.stop), "%H:%M:%S")
        converted_start = datetime.datetime.strptime(str(task.start), "%H:%M:%S")
        if converted_stop < converted_start:
            length = converted_stop - converted_start + timedelta(hours=24)
        else:
            length = converted_stop - converted_start
        task.finished_on = tzware_datetime()
        task.length = length
        task.user = initials
        task.user_id = id_user
        task.save()

    data = ast.literal_eval(data)

    return redirect(url_for('daily_checklist.checklist_do', **data))