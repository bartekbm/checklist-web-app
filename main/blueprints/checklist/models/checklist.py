from flask import flash
from flask_login import current_user

from lib.util_datetime import tzware_datetime
from lib.util_sqlalchemy import ResourceMixin, AwareDateTime
from main.blueprints.admin.models import Roles
from main.blueprints.checklist.models.checklist_template import ChecklistTemplate
from main.extensions import db


class Checklist(db.Model, ResourceMixin):
    __tablename__ = 'checklists'
    collation = 'utf8_general_ci'
    id = db.Column(db.Integer, autoincrement=True, primary_key=True)
    created_on = db.Column(AwareDateTime(),
                           default=tzware_datetime)
    name = db.Column(db.String(60))

    RoleID = db.Column(db.Integer, db.ForeignKey('checklists_templates.RoleID', ondelete='SET NULL',onupdate='CASCADE'))
    ChecklistTemplateID = db.Column(db.Integer, db.ForeignKey('checklists_templates.id', ondelete='SET NULL',onupdate='CASCADE'))
    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(Checklist, self).__init__(**kwargs)

    @classmethod
    def get_template_id(cls, template_id):
        result = ChecklistTemplate.get_by_checklist_template_id(template_id)
        name = [i.name for i in result]
        RoleID = [i.RoleID for i in result]
        return name, RoleID

    @classmethod
    def get_by_user_group(cls, roles):
        return Checklist._get_by_user_group(roles)

    @classmethod
    def _get_by_user_group(cls, roles):
        query = db.session.query(Checklist).filter(Checklist.RoleID.in_(roles))
        return query


    @classmethod
    def create(cls, params):
        """


        :return: bool
        """

        checklist = Checklist(**params)
        session = db.session
        session.add(checklist)
        session.commit()
        return checklist.id

    @classmethod
    def search(cls, query):


        if not query:
            return ''
        search_query = f'%{query}%'
        search_chain = (Checklist.name.ilike(search_query))

        return search_chain

    @classmethod
    def bulk_delete(cls, ids):

        delete_count = 0

        for id in ids:
            checklist = Checklist.query.get(id)

            if Checklist is None:
                continue

            checklist.delete()
            delete_count += 1

        return delete_count

    @classmethod
    def check_if_authorised(cls, id):
        roles = Roles.get_specify_role_by_user(current_user.id)
        role_verification = [role.id for role in roles]
        checklist = cls.query.get(id)
        try:
            if checklist.RoleID not in role_verification:
                flash('Nie masz wystarczających uprawnień.', 'error')
                return True
        except AttributeError:
            flash('Nie masz wystarczających uprawnień.', 'error')
            return True


