from sqlalchemy.dialects.mysql import TIME

from lib.util_sqlalchemy import ResourceMixin
from main.blueprints.checklist.models.task_template import TaskTemplate
from main.extensions import db
from lib.util_sqlalchemy import AwareDateTime



class Task(db.Model, ResourceMixin):
    __tablename__ = 'tasks'
    collation = 'utf8_general_ci'
    id = db.Column(db.Integer, primary_key=True)
    user = db.Column(db.String(24))
    user_id = db.Column(db.Integer)
    finished_on = db.Column(AwareDateTime())
    start = db.Column(TIME())
    stop = db.Column(TIME())
    length = db.Column(TIME())
    ChecklistId = db.Column(db.Integer, db.ForeignKey('checklists.id', ondelete='CASCADE'))
    TaskTemplateID = db.Column(db.Integer, db.ForeignKey('tasks_template.id', ondelete='CASCADE'))

    @classmethod
    def get_by_checklist_id(cls, id):
        return Task._get_by_checklist_id(id)

    @classmethod
    def _get_by_checklist_id(cls, id):
        query = db.session.query(Task.id,Task.user, Task.start, Task.stop, Task.length,
                                 TaskTemplate.name, TaskTemplate.a_when, TaskTemplate.description, TaskTemplate.who
                                 ).outerjoin(TaskTemplate).\
            filter(Task.TaskTemplateID == TaskTemplate.id).filter(Task.ChecklistId == id)
        return query


    @classmethod
    def create(cls, params):


        checklist = Task(**params)

        db.session.add(checklist)
        db.session.commit()

        return True


    @classmethod
    def sort_by(cls, field, direction):

        return field, direction


    @classmethod
    def search(cls, query):


        if not query:
            return ''
        search_query = f'%{query}%'
        print(query)
        search_chain = (TaskTemplate.name.ilike(search_query))

        return search_chain

    @classmethod
    def get_from_template(cls, id):
        pass