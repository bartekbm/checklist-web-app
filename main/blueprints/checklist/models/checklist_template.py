from flask import flash
from flask_login import current_user

from lib.util_datetime import tzware_datetime
from lib.util_sqlalchemy import AwareDateTime, ResourceMixin
from main.blueprints.admin.models import Roles
from main.extensions import db


class ChecklistTemplate(db.Model, ResourceMixin):
    collation = 'utf8_general_ci'
    __tablename__ = 'checklists_templates'
    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(AwareDateTime(),
                           default=tzware_datetime)
    name = db.Column(db.String(24), index=True)
    RoleID = db.Column(db.Integer, db.ForeignKey('roles.id', ondelete='CASCADE'),
                       nullable=False)

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(ChecklistTemplate, self).__init__(**kwargs)

    @classmethod
    def get_by_user_group(cls, roles):
        return ChecklistTemplate._get_by_user_group(roles)

    @classmethod
    def _get_by_user_group(cls, roles):
        query = db.session.query(ChecklistTemplate).filter(ChecklistTemplate.RoleID.in_(roles))
        return query

    @classmethod
    def get_by_checklist_template_id(cls, id):
        return ChecklistTemplate._get_by_checklist_template_id(id)

    @classmethod
    def _get_by_checklist_template_id(cls, id):
        query = ChecklistTemplate.query.filter(ChecklistTemplate.id == id)
        return query
    @classmethod
    def search(cls, query):


        if not query:
            return ''
        search_query = f'%{query}%'
        search_chain = (ChecklistTemplate.name.ilike(search_query))

        return search_chain

    @classmethod
    def create(cls, params):

        check_list_template = ChecklistTemplate(**params)

        db.session.add(check_list_template)
        db.session.commit()

        return True

    @classmethod
    def bulk_delete(cls, ids):

        delete_count = 0

        for id in ids:
            checklist = ChecklistTemplate.query.get(id)

            if ChecklistTemplate is None:
                continue

            checklist.delete()
            delete_count += 1

        return delete_count

    @classmethod
    def check_if_authorised(cls, checklists_templates_id):
        roles = Roles.get_specify_role_by_user(current_user.id)
        role_verification = [role.id for role in roles]
        checklist_template = cls.query.get(checklists_templates_id)
        try:
            if checklist_template.RoleID not in role_verification:
                flash('Nie masz wystarczających uprawnień.', 'error')
                return True
        except AttributeError:
            flash('Nie masz wystarczających uprawnień.', 'error')
            return True



