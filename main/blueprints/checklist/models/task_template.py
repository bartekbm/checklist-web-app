from sqlalchemy.dialects.mysql import TIME, TEXT


from lib.util_sqlalchemy import AwareDateTime, ResourceMixin
from main.extensions import db
from lib.util_datetime import tzware_datetime

class TaskTemplate(db.Model, ResourceMixin):

    __tablename__ = 'tasks_template'
    collation = 'utf8_general_ci'
    id = db.Column(db.Integer, primary_key=True)
    created_on = db.Column(AwareDateTime(),
                           default=tzware_datetime)

    name = db.Column(db.String(60))

    a_when = db.Column(TIME())
    description = db.Column(TEXT())
    who = db.Column(db.String(7))
    #daily_task = db.Column(db.Boolean(), nullable=False,
                       #server_default='1') Project not prepared for daily task
    checklists_templates_id = db.Column(db.Integer, db.ForeignKey('checklists_templates.id', ondelete='CASCADE'),
                       nullable=False)

    def __init__(self, **kwargs):
        # Call Flask-SQLAlchemy's constructor.
        super(TaskTemplate, self).__init__(**kwargs)

    @classmethod
    def get_by_checklist_id(cls, id):
        return TaskTemplate._get_by_checklist_id(id)

    @classmethod
    def _get_by_checklist_id(cls, id):
        query = TaskTemplate.query.filter(TaskTemplate.checklists_templates_id == id)
        return query

    @classmethod
    def search(cls, query):

        if not query:
            return ''
        search_query = f'%{query}%'

        search_chain = (TaskTemplate.name.ilike(search_query))

        return search_chain


    @classmethod
    def bulk_delete(cls, ids):

        delete_count = 0

        for id in ids:
            tasks = TaskTemplate.query.get(id)

            if TaskTemplate is None:
                continue

            tasks.delete()
            delete_count += 1

        return delete_count


    @classmethod
    def create(cls, params):

        task = TaskTemplate(**params)

        db.session.add(task)
        db.session.commit()

        return task.id