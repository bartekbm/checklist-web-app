from sqlalchemy_utils import database_exists, create_database
from main.app import create_app
from main.extensions import db
from main.blueprints.user.models import User


app = create_app()
db.app = app



def init(with_testdb):
    db.drop_all()
    app.config['MYSQL_DATABASE_CHARSET'] = 'utf8mb4'
    db.create_all()
    if with_testdb:
        db_uri = f"{app.config['SQLALCHEMY_DATABASE_URI']}_test"

        if not database_exists(db_uri):
            create_database(db_uri)

    return None

def seed(new_pass):
    try:
        if User.find_by_identity(app.config['SEED_ADMIN_EMAIL']) is not None:
            return None
    except:
        print("Propably configuration.ini is not generated, exiting ...")
        exit(-1)
    print(new_pass)
    params = {
        'role': 'admin',
        'email': app.config['SEED_ADMIN_EMAIL'],
        'password': new_pass
    }
    return User(**params).save()

def reset(new_pass,with_testdb=False):
    init(with_testdb)
    seed(new_pass)
    print("Database initialized ..., password saved to file, admin login: dev@local.host")


