
import setup_app


def save_file():
    try:
        from config.settings import LOG_LEVEL, SERVER_PORT, SERVER_FULL_NAME
    except:
        print("Propably configuration.ini is not generated, exiting ...")
        exit(-1)
    NAME = 'CHECKLIST_APP'
    PROJECTDIR = setup_app.PROJECT_ROOT
    NUM_WORKERS = 2  # https://docs.gunicorn.org/en/stable/faq.html#faq
    WSGI_MODULE = 'run_app:create_app(run=True)'

    start = f'''gunicorn "{WSGI_MODULE}" \
    --name {NAME} \
    --workers {NUM_WORKERS} \
    --timeout 300 \
    --bind={SERVER_FULL_NAME}:{SERVER_PORT} \
    --log-level={LOG_LEVEL} \
    --error-logfile={setup_app.PROJECT_ROOT}/logs/checklist_app.log \
    --capture-output \
    --daemon
    '''

    run_gunicorn = f'''
    #!/bin/bash
    source ./virtual_env/bin/activate
    startme() {{ { start } }}
    stopme() {{
    kill -9 $(pgrep -f {NAME})
    }}
    case "$1" in
        start)   startme ;;
        stop)    stopme ;;
        restart) stopme; startme ;;
        *) echo "usage: $0 start|stop|restart" >&2
           exit 1
           ;;
        esac
    '''

    with open("checklist_app.sh", 'w+') as f:
        f.write(run_gunicorn)
