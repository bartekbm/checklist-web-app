import setup_app
import io
import configparser, os.path
from pathlib import Path
import secrets
import string

config = configparser.ConfigParser()


cfg_path = Path(f'{setup_app.PROJECT_ROOT}/configuration.ini')


required = {'Server': ['DEBUG', 'LOG_LEVEL', 'SERVER_NAME', 'SERVER_PORT', 'SECRET_KEY', ],
            'Database': ['db_host', 'db_port', 'db_name', 'db_user', 'db_password',
                         'SQLALCHEMY_TRACK_MODIFICATIONS', 'SQLALCHEMY_ECHO']}

default_config = Path(f"{setup_app.PROJECT_ROOT}/config/configuration_template.cfg")
seed_admin_password = Path(f'{setup_app.PROJECT_ROOT}/seed_admin_password_save_it_AND_DELETE_FILE')


def generate_random_password_save_to_file():
    alphabet = string.ascii_letters + string.digits
    password = ''.join(secrets.choice(alphabet) for i in range(20))
    TO_SAVE = f'[pass]\nSEED_ADMIN_PASSWORD = {password}'
    with open(seed_admin_password, 'w') as save_pass:
        save_pass.write(TO_SAVE)
    return TO_SAVE

def read_seed_password(password):
    read_password = configparser.ConfigParser()
    buf = io.StringIO(password)
    read_password.read_file(buf)
    ps = read_password._sections
    return ps['pass']['seed_admin_password']


def read_default_config():
    print('Start reading')
    read_default = configparser.ConfigParser()
    if not os.path.exists(default_config):
        print(f'Problem with reading template of configuration file, this file should be there: {default_config}, '
              'you can download it from git repository ')
        exit(-1)
    open_file = open(default_config)
    read_default.read_file(open_file)
    template_config = []
    for each in read_default.sections():
        for (key, val) in read_default.items(each):
            to_append = {each: {key: val.split(',')}}
            template_config.append(to_append)
    open_file.close()
    return template_config


def save_new_config_file(data):
    if os.path.exists(cfg_path):
        print(f'{cfg_path} file exist, but should be deleted, exiting')
        exit(-1)
    save_config = configparser.ConfigParser()
    sections = []
    for sec in data:
        for section in sec.keys():
            sections.append(section)
    sections = list(dict.fromkeys(sections))

    for section in sections:
        save_config.add_section(section)

    for one_line in data:

        for section, value in one_line.items():

            for option, in_value in value.items():
                text = f"Input for section: {section}, option: {option.upper()}, required type: {in_value[0]}," \
                       f" default(this will be set if not given): {in_value[1]}, more info: {in_value[2]}.\n" \
                       f"Your input: "
                user = input(text)

                if in_value[0] == 'bool':
                    set_instance = bool
                if in_value[0] == 'str':
                    set_instance = str
                if in_value[0] == 'int':
                    set_instance = int

                if user == '':
                    print(f'Empty answer, setting default {in_value[1]}')
                    user = in_value[1]

                try:
                    user = eval(user)
                except:
                    pass

                while not isinstance(user, set_instance):

                    user = input(f'Wrong type, should be {in_value[0]}')
                    if user == '':
                        print(f'Empty answer, setting default {in_value[1]}')
                        user = in_value[1]

                    try:
                        user = eval(user)
                    except:
                        pass

                print(f'OK - set {user}')
                save_config[section][option.upper()] = str(user)

    with open(cfg_path, 'w') as configfile:
        save_config.write(configfile)

    return True


def check_config():
    if not os.path.exists(cfg_path):
        return 'Configuration file not exist, create config file or run first_setup.py'
    open_file = open(cfg_path)
    config.read_file(open_file)
    check_section = required.keys()
    for section in check_section:
        if not config.has_section(section):
            return f"Configuration don't have section: {section}."

    for section, option in required.items():
        for one in option:
            if not config.has_option(section, one):
                return f"Section {section}, don't have option {one}"
    open_file.close()
    return True


def read_config():
    if check_config() is True:
        configuration = config._sections
        return configuration
    else:
        return check_config()


def delete_config():
    if os.path.exists(cfg_path):
        os.remove(cfg_path)
    else:
        print(f"Something goes wrong with file {cfg_path}")
