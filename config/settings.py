from datetime import timedelta
from config.config_parser import read_config, read_seed_password
from ast import literal_eval

settings = read_config()


def check_settings():
    if not isinstance(settings, dict):
        return settings


try:
    # Server
    DEBUG = literal_eval(settings['Server']['debug'])
    FLASK_DEBUG = literal_eval(settings['Server']['flask_debug'])
    LOG_LEVEL = settings['Server']['log_level']
    SERVER_FULL_NAME = settings['Server']['server_name']
    SERVER_PORT = settings['Server']['server_port']
    #SERVER_NAME = SERVER_FULL_NAME + ":" + SERVER_PORT
    SECRET_KEY = settings['Server']['secret_key']
    REMEMBER_COOKIE_DURATION = timedelta(
        hours=int(settings['Server']['remember_cookie_duration']))  # can log in to website 90 days
    db_uri = f"mysql+pymysql://{settings['Database']['db_user']}:{settings['Database']['db_password']}@{settings['Database']['db_host']}:{settings['Database']['db_port']}/{settings['Database']['db_name']}"
    SQLALCHEMY_DATABASE_URI = db_uri
    SQLALCHEMY_TRACK_MODIFICATIONS = literal_eval(settings['Database']['sqlalchemy_track_modifications'])
    SQLALCHEMY_ECHO = literal_eval(settings['Database']['sqlalchemy_echo'])
    SEED_ADMIN_EMAIL = 'dev@local.host'

    # Mails
    MAIL_DEFAULT_SENDER = settings['Mail']['mail_username']
    MAIL_SERVER = settings['Mail']['mail_server']
    MAIL_PORT = literal_eval(settings['Mail']['mail_port'])
    MAIL_USE_TLS = literal_eval(settings['Mail']['mail_use_tls'])
    MAIL_USE_SSL = literal_eval(settings['Mail']['mail_use_ssl'])
    MAIL_USERNAME = settings['Mail']['mail_username']
    MAIL_PASSWORD = settings['Mail']['mail_password']
    MAIL_DEBUG = literal_eval(settings['Mail']['mail_debug'])
    # Timezone
    TIMEZONE = settings['Timezone']['timezone']
except TypeError:
    pass
