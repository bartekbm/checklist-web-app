import datetime

import pytz
try:
    from config.settings import TIMEZONE
except ImportError or TypeError:
    TIMEZONE = 'Europe/Warsaw'


def tzware_datetime():

    return datetime.datetime.now(pytz.timezone(TIMEZONE))


def checklist_date():

    today_date = datetime.datetime.now()

    check_list = str(today_date.strftime("%A %d %B %Y "))
    return str(check_list).title()


def timedelta_months(months, compare_date=None):

    if compare_date is None:
        compare_date = datetime.date.today()

    delta = months * 365 / 12
    compare_date_with_delta = compare_date + datetime.timedelta(delta)

    return compare_date_with_delta
