from sqlalchemy import DateTime, and_
from sqlalchemy.types import TypeDecorator

from main.extensions import db


class AwareDateTime(TypeDecorator):

    impl = DateTime(timezone=False)



    def __repr__(self):
        return 'AwareDateTime()'




class ResourceMixin(object):

    @classmethod
    def sort_by(cls, field, direction):

        if field not in cls.__table__.columns:
            field = 'created_on'

        if direction not in ('asc', 'desc'):
            direction = 'asc'

        return field, direction

    @classmethod
    def get_bulk_action_ids(cls, scope, ids, roles=[], checklists_templates_id=None, omit_ids=[], query=''):

        omit_ids = map(str, omit_ids)

        if scope == 'all_search_results':
            # Change the scope to go from selected ids to all search results.
            if roles:
                ids = cls.query.with_entities(cls.id).filter(and_(cls.search(query),cls.RoleID.in_(roles)))
            elif checklists_templates_id:
                ids = cls.query.with_entities(cls.id).filter(and_(cls.search(query),
                                                                  cls.checklists_templates_id == checklists_templates_id
                                                                  ))
            else:
                ids = cls.query.with_entities(cls.id).filter(cls.search(query))
            # SQLAlchemy returns back a list of tuples, we want a list of strs.
            ids = [str(item[0]) for item in ids]


        # Remove 1 or more items from the list, this could be useful in spots
        # where you may want to protect the current user from deleting themself
        # when bulk deleting user accounts.
        if omit_ids:
            ids = [id for id in ids if id not in omit_ids]

        return ids

    @classmethod
    def bulk_delete(cls, ids):

        delete_count = cls.query.filter(cls.id.in_(ids)).delete(
            synchronize_session=False)
        db.session.commit()

        return delete_count

    def save(self):


        db.session.add(self)

        db.session.commit()

        return self


    def delete(self):

        db.session.delete(self)
        return db.session.commit()

    def __str__(self):

        obj_id = hex(id(self))
        columns = self.__table__.c.keys()

        values = ', '.join("%s=%r" % (n, getattr(self, n)) for n in columns)
        return '<%s %s(%s)>' % (obj_id, self.__class__.__name__, values)
