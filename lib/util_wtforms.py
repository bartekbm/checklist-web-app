from flask_wtf import FlaskForm


class ModelForm(FlaskForm):

    def __init__(self, obj=None, prefix='', **kwargs):
        FlaskForm.__init__(
            self, obj=obj, prefix=prefix, **kwargs
        )
        self._obj = obj


def choices_from_dict(source, prepend_blank=True):

    choices = []

    if prepend_blank:
        choices.append(('', 'Proszę wybrać jeden...'))

    for key, value in source.items():
        pair = (key, value)
        choices.append(pair)

    return choices


def choices_from_list(source, prepend_blank=True):

    choices = []

    if prepend_blank:
        choices.append(('', 'Proszę wybrać jeden...'))

    for item in source:
        pair = (item, item)
        choices.append(pair)

    return choices
