# Table of contents

* [Checklist web-app](#Checklist-web-app)
* [Getting Started](#Getting-Started)
* [Built With](#Built-With)
* [Requirements](#Requirements)
* [Prerequisites](#Prerequisites)
    * [MySQL](#MySQL)
    * [Get app](#Get-app)
* [Installation](#Installation)
* [After installation](#After-installation)
* [Basic usage/tips](#Basic-usage)
* [License](#License)

<a name="Checklist-web-app"></a>
# Checklist web-app 

<p align="center">
    <a href="https://gitlab.com/bartekbm/checklist-web-app/-/pipelines" alt="Build status">
        <img src="https://img.shields.io/gitlab/pipeline/bartekbm/checklist-web-app/demo.svg" /></a>
    <a href="https://gitlab.com/bartekbm/checklist-web-app/-/blob/master/LICENSE.txt" alt="License">
        <img src="https://img.shields.io/badge/License-MIT-yellow.svg" /></a>
    <a href="https://gitlab.com/bartekbm/checklist-web-app/-/tags" alt="Version">
        <img src="https://img.shields.io/badge/version-1.0.0-blue" /></a>
</p>


> Cool checklist management, with templates, user groups and reports ...

At the moment, the application is only in Polish.

**INFO: Looking for new server and domain, so for now app is not available.**

You can test this app in there: https://checklist.bartekb.space. Feel free to mess around :)

App and database is restarted everyday around midnight. Login and password to admin account is generated and shown in login
page https://checklist.bartekb.space/login - this feature ONLY on demo branch.

You can take a peek at demo pipeline (deploy, generate fake data and run) there: 
https://gitlab.com/bartekbm/checklist-web-app/-/pipelines

<a name="Getting-Started"></a>
# Getting Started

Every step of basic install you can check in CI/CD of project 
https://gitlab.com/bartekbm/checklist-web-app/-/blob/demo/.gitlab-ci.yml

<a name="Built-With"></a>
## Built With

Project is created with:
* Python 3.6
* Bootstrap 4
* Flask and related flask libraries like Flask-mail or Flask-login
* SQLAlchemy ORM
* Jinja2 to generate html content

<a name="Requirements"></a>
## Requirements

Installation was tested on linux:
* centos 6/7/8
* red hat 7


Basic requirements to run:
* Python 3.6 or higher ( setup will search for python in /usr/bin/python and /usr/local/bin/python3 )
* MySQL (tested on MySQL 8.0 and MariaDB 10.5)

Web browser:
* Opera
* Chrome
* Edge

Mozilla, Internet Explorer and Safari for Mac will not support "calendar" in reports section

<a name="Prerequisites"></a>
## Prerequisites

<a name="MySQL"></a>
### MySQL

Create database with correct charset utf8_unicode (default name of db for app is checklist_app).


```
CREATE DATABASE name_of_db DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
```

Next create admin user with password, and grant privileges to created db. Remember to save password, this will be needed
in setup step.


```
CREATE USER 'checklist_admin'@'localhost' IDENTIFIED BY 'your_secret_password';
GRANT ALL PRIVILEGES ON name_of_db.* TO 'checklist_admin'@'localhost';
FLUSH PRIVILEGES;
```


<a name="Get-app"></a>
### Get app
Get app by downloading and unpacking source code 


```
mkdir location_where_app_will_be_installed
curl https://gitlab.com/bartekbm/checklist-web-app/-/archive/master/checklist-web-app-master.tar.gz -o checklist-web-app-master.tar.gz # or copy via ftp 
cp ./checklist-web-app-master.tar.gz ./location_where_app_will_be_installed
cd location_where_app_will_be_installed
tar -xzf checklist-web-app-master.tar.gz
cd checklist-web-app-master
```


or via git


```
git clone https://gitlab.com/bartekbm/checklist-web-app.git
```


<a name="Installation"></a>
## Installation


First make ```install_packages.sh``` executable
```
cd location_where_app_will_be_installed/checklist-web-app-master
chmod +x install_packages.sh
```
And then run script to install all requirements for app. Project have all dependencies as offline packages. 
(folder offline_packages)
```
./install_packages.sh
```
After installation you need activate created virtual environment
```
source ./virtual_env/bin/activate
```
Now we can run our first setup for app.  
```
python first_setup.py
```
There are 3 options. Initiate new configuration.ini, recreate tables in db, create starting script for app. Input N  to go to the next step 
* FIRST: App ask for necessary settings (four sections: Server, Database, Mail, Timezone), if you will input empty string (enter key) app will set default options, not 
recommended for passwords and users. Recommended for debug and login options. After generating this file you can manually
edit configuration.ini and then restart app.
* SECOND: **WARNING**. This will DELETE ALL in database and recreate. Recommended only on the first run. It will create
admin user, password and save to file seed_admin_password_save_it_AND_DELETE_FILE. Remember to save password and **DELETE**
this file after. If you don't anybody who has access to server will be admin of this app (or change password of this user).
* THIRD: This will create new bash file ```checklist_app.sh``` to start app.

After the previous step, we can run the application.
```
./checklist_app.sh start
```
There is also options to restart and stop app.
```
./checklist_app.sh stop
./checklist_app.sh restart
```


<a name="After-installation"></a>
## After installation


Stared app will be listen on port defined in configuration.ini. Row SERVER_PORT. We can use apache or nginx to handle request for app.
Example nginx [configuration](./example_nginx.conf) that will pass requests for app to port 80:


```
server {
        listen 80;
        listen [::]:80;
        server_name checklist.bartekb.space;

        location / {
                    proxy_pass http://localhost:5000;
                    proxy_set_header X-Forwarded-For $remote_addr;
  }
}
```


<a name="Basic-usage"></a>
## Basic Usage/Tips


After login to app with admin account, create own account with your email and password, and after that set your account as admin.
Generated admin don't have name, surname or nick. After this create group and even admin should be added to new created groups
https://checklist.bartekb.space/admin/users


<a name="License"></a>
## License


[MIT](./LICENSE.txt)
